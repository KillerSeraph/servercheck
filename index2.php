<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Server Report</title>

    <!-- This information is required by bootstrap in order for it to function correctly -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>

<body>

    <script type="text/javascript">
        // Client ID and API key from the Developer Console
        var CLIENT_ID = '479677517330-l92oarh0bieu7qr5ltag4239adr4e3n2.apps.googleusercontent.com';

        // Array of API discovery doc URLs for APIs used by the quickstart
        var DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/gmail/v1/rest"];

        // Authorization scopes required by the API; multiple scopes can be
        // included, separated by spaces.
        //compose = Create, read, update, and delete drafts. Send messages and drafts.
        var SCOPES = 'https://www.googleapis.com/auth/gmail.compose';

        alert(document.getElementById('authorize-button'));
        var authorizeButton = document.getElementById('authorize-button');
        var signoutButton = document.getElementById('signout-button');

        /**
         *  On load, called to load the auth2 library and API client library.
         */
        function handleClientLoad() {
            gapi.load('client:auth2', initClient);
        }

        /**
         *  Initializes the API client library and sets up sign-in state
         *  listeners.
         */
        function initClient() {
            gapi.client.init({
                discoveryDocs: DISCOVERY_DOCS,
                clientId: CLIENT_ID,
                scope: SCOPES
            }).then(function () {
                // Listen for sign-in state changes.
                gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);

                // Handle the initial sign-in state.
                updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
            });
        }


        /**
         * Append a pre element to the body containing the given message
         * as its text node. Used to display the results of the API call.
         *
         * @param {string} message Text to be placed in pre element.
         */
        function appendPre(message) {
            var pre = document.getElementById('content');
            var textContent = document.createTextNode(message + '\n');
            pre.appendChild(textContent);
        }

    </script>

    <script async defer src="https://apis.google.com/js/api.js"
            onload="this.onload=function(){};handleClientLoad()"
            onreadystatechange="if (this.readyState === 'complete') this.onload()">
    </script>

    <?php

        class Server {

            protected $location;
            protected $rackNumber;
            protected $slots;
            protected $harddriveBays;
            protected $name;
            protected $tagNumber;
            protected $serialNumber;
            protected $make;
            protected $model;
            protected $comment;


            function Server() {
                echo "";
            }

            public function SetLocation( $theLocation )
            {
                $this->location = $theLocation;
            }

            public function SetRackNumber( $theRackNumber )
            {
                $this->rackNumber = $theRackNumber;
            }

            public function SetSlots( $theSlots )
            {
                $this->slots = $theSlots;
            }

            public function SetHDBays( $theHDBays )
            {
                $this->harddriveBays = $theHDBays;
            }

            public function SetName( $theName )
            {
                $this->name = $theName;
            }

            public function SetTagNumber( $theTagNumber )
            {
                $this->tagNumber = $theTagNumber;
            }

            public function SetSerialNumber( $theSerialNumber )
            {
                $this->serialNumber = $theSerialNumber;
            }

            public function SetMake( $theMake )
            {
                $this->make = $theMake;
            }

            public function SetModelNumber( $theModelNumber )
            {
                $this->model = $theModelNumber;
            }

            public function SetComment( $theComment )
            {
                $this->comment = $theComment;
            }
        }

        //print_r($_POST);

        $sortedArray;

        foreach ( sizeof( $_POST )  as $i )
        {
                array_push($sortedArray, ${'array'.$i} =[]);
        }

        $innerCounter = 0;
        $outerCounter = 0;

        foreach ( $_POST as $value)
        {

            if( gettype($value) === "array" )
            {
                foreach ( $value as $subvalue)
                {
                    array_push( ${'array'.$innerCounter},  $subvalue );
                    $innerCounter++;

                    //reset the inner counter
                    if($innerCounter == sizeof( $_POST ) )
                    {
                        $innerCounter = 0;
                    }
                }

            }
            else
            {
                echo "You shouldn't be here. This is an error (Isn't it pretty?)";
            }
        }

        echo print_r($array1);

        echo print_r($_POST);
        echo '\n';
        echo '\n';

    foreach ($_POST as $key => $value) {
        echo "<tr>";
        echo "<td>";
        echo $key;
        echo "</td>";
        echo "<td>";
        echo $value;
        echo "</td>";
        echo "</tr>";
    }

    ?>
</body>
</html>
